#define Ntap 50

class Bessel
{
public:
  Bessel();
  ~Bessel();
  float fir(float newSample);

private:
  float x[Ntap]; //input samples

};

