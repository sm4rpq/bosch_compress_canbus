At varmepumpsforum there was someone who had tested every single value and checked what each
A/D-value translates to on the display. That table is the base for all the files here.

temp_tabeller.ods is some checking done in open office to see how to translate values into temperature
based on that work.

temps.csv is the original table
temps_new.csv is an extended table with the first step of the calculations done.
A/D readings are based on a fixed resistor to Vref of the A/D and the NTC to ground.
A high temperature=>a low resistance of the NTC, which gives a low voltage into the A/D.
A low temperature=>a high resistance of the NTC, which gives a high voltage into the A/D.


I did not like the idea of having the full translation table in my software so I made a curve fit to
get parameters for a NTC function. The I used gnuplot to do the curve fitting.

gnuplot>
f(x)=1/(a+b*log(x)+c*log(x)*log(x));
fit[.2:10] f(x) 'temps_new.csv' u 3:5 via a,b,c;
print a
print b
print c


