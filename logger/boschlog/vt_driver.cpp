#include "vt_driver.h"

//#define VTDEBUG

///////////////////////////////////
// Constructor
///////////////////////////////////

vtDriver::vtDriver(Stream *rootStream, int bufSize)
{
    p_rootStream=rootStream;
    m_bufSize = bufSize;
    m_bufSpace = bufSize-1;

    buf=(char*)malloc(sizeof(char) * m_bufSize);
    
    m_parseMode = insert;
    editpoint=0; //Insertion point, this is where the next character entered will go.
    endpoint=-1; //Index of last character in string. Is -1 if no characters, Is set == m_bufSpace if string is full.
    insertmode=true;
    csichar = '\x00';
    m_enableEcho = true;
}

void vtDriver::parse()
{
  while (p_rootStream->available()) {
    //Get character from serial port
    char c = p_rootStream->read();

    switch(m_parseMode)
    {
      case insert:
        parseIns(c);
        break;
      case ESC:
        parseEsc(c);
        break;
      case CSI:
        parseCSI(c);
        break;
      case DCS:
        parseDCS(c);
        break;
      case SS3:
        m_parseMode=insert;
        break;
    }
  }
}

void vtDriver::cls()
{
  p_rootStream->print("\x1B[H\x1B[J");
}

void vtDriver::gotoXY(int x, int y)
{
  p_rootStream->print("\x1B[");
  p_rootStream->print(y);
  p_rootStream->print(';');
  p_rootStream->print(x);
  p_rootStream->print("H");
}

char vtDriver::getLastChar()
{
  parse();
  return m_lastChar; 
}

String vtDriver::getInputLine()
{
  do
    parse();
  while (m_lastChar!=13);
  m_lastChar=0;
  return m_lastLine;
}

void vtDriver::enableEcho(bool bEnable)
{
  m_enableEcho = bEnable;
}

///////////////////////////////////
// Receive character and handle
///////////////////////////////////
void vtDriver::parseIns(char c)
{
  //Depending on which character different things should happen.
  switch(c)
  {
    case 3: //Ctrl-C
      m_lastChar = 3;
    case 8: //BkSp
      if (editpoint>0){
        //Move everything from the insertion point and to the right one step to the left.
        for (int i=editpoint; i<=endpoint+1; i++)
        {
          buf[i-1]=buf[i];
        }
        p_rootStream->print("\x1B[1D"); //Move cursor to the left
        p_rootStream->print("\x1B[1P"); //Delete character
        editpoint=editpoint-1;
        endpoint=endpoint-1;
     }
     break;
    case 13: //Cr
      if (m_enableEcho)
        p_rootStream->print(c);
      #ifdef VTDEBUG
        p_rootStream->print("Command:");
        p_rootStream->println(buf);
        if (insertmode)
          p_rootStream->println("Insert mode");
        else
          p_rootStream->println("Overwrite mode");
      #endif
      m_lastLine=String(buf);
      buf[0]='\x00';
      editpoint=0;
      endpoint=-1;
      m_lastChar=13;
      break;
   case 27: //Esc
     m_parseMode=ESC;
     break;
   case 127: //Delete
     if (editpoint<=endpoint)
     {
       p_rootStream->print("\x1B[1P"); //Delete character
       //Move everything after the insertion point and to the right one step to the left.
       for (int i=editpoint; i<=endpoint; i++)
       {
         buf[i]=buf[i+1];
       }
       endpoint=endpoint-1;
     }
     m_lastChar=127;
     break;
   case 143:
     m_parseMode=SS3;
   case 144:
     m_parseMode=DCS;
     break;
   case 155:
     m_parseMode=CSI;
     break;
   default:
     if (endpoint<m_bufSpace){ //Can only insert if room in the buffer. m_bufSpace is one less than m_bufSize and endpoint needs to be before buflen to allow insertion.
       //Move everything from the insetion point and onwards one step to the right (insert)
       //This includes the terminating \0
       if (insertmode) {
         for (int i=endpoint+1; i>=editpoint; i--)
         {
           buf[i+1]=buf[i];
         }
         endpoint=endpoint+1;
       }
       buf[editpoint]=c;
       if (editpoint>endpoint) //Added to end
       {
         buf[editpoint+1]='\x00';
         endpoint=endpoint+1;
       }
       editpoint=editpoint+1;
       if (m_enableEcho) {
         if (insertmode)
           p_rootStream->print("\x1B[1@"); //Insert character
         p_rootStream->print(c);
       }
       m_lastChar=c;
     }
     else
     {
       p_rootStream->print('\x07'); //Beep
     }
  }
}

///////////////////////////////////
// Esc character previously received
// Interpret next character
///////////////////////////////////
void vtDriver::parseEsc(char c)
{
  switch(c)
  {
    case '[':
      m_parseMode=CSI; //Command Sequence Introducer
      break;
    case 'P' :
      m_parseMode=DCS; //Device Control String
      break;
    default:
      m_parseMode=insert;
  }
}

///////////////////////////////////
// CSI (Command Sequence Introducer) previously received
// Interpret next character
///////////////////////////////////
void vtDriver::parseCSI(char c)
{
  switch(c)
  {
    case 'A' : //Up key
      m_lastLine.toCharArray(buf,m_bufSize);
      m_parseMode=insert;
      p_rootStream->print("\x1b[132D\x1b[1K");
      p_rootStream->print(buf);
      editpoint=m_lastLine.length();
      endpoint=editpoint-1;
      break;
    case 'B' : //Down key
      buf[0]=0;
      editpoint=0;
      endpoint=-1;
      m_parseMode=insert;
      p_rootStream->print("\x0d\x1b[2K");
      break;
    case 'C' : //Arrow right
      if (editpoint<endpoint+1)
      {
        editpoint=editpoint+1;
        p_rootStream->print("\x1B[1C");
      }
      m_parseMode=insert;
      break;
    case 'D' : //Arrow left
      if (editpoint>0)
      {
        editpoint=editpoint-1;
        p_rootStream->print("\x1B[1D");
      }
      m_parseMode=insert;
      break;
    case '~': // Ends some functions
      switch(csichar)
      {
        case '1': //Home
          editpoint=0;
          p_rootStream->print('\x0D'); //Cr - positions cursor to the left
          break;
        case '2': //Ins
          insertmode=!insertmode;
          break;
        case '3': //Remove
          break; //Not used on PC-keyboard, at least not mapped by teraterm
        case '4': //End
          for (int i=editpoint;i<endpoint+1;i++)
            p_rootStream->print(buf[i]);
          editpoint=endpoint+1;
          break;
        case '5': //Prev screen (Page Up)
          break;
        case '6': //Next screen (Page Down)
          break;
      }
      m_parseMode=insert;
      break;
    default:
      csichar = c;
  }
}

///////////////////////////////////
// DCS (Device Control String) previously received
// Interpret next character
///////////////////////////////////
void vtDriver::parseDCS(char c)
{
  switch(c)
  {
    default:
      m_parseMode=insert;
  }
}



