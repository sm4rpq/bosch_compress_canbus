Logfile is GVRET format for reading in savvycan

pump1.csv - First capture. useconds in timestamps
pump3.csv - Long capture (34hours) milliseconds in timestamps
pump5.csv - Reduced clutter capture. No duplicates recorded. When changes occurr last value also included with
            new timestamp. Experiments with turning on/off and disconnecting the front panel just logging the
            BAS unit.
pump7.csv - Introduced absolute timestamps using "fake" id 0. Experiments with the external inputs and the
            unused analog input.
pump_set_power_off.csv - Capture when turning the heat pump off (nothing special detected, strange)
pump_set_power_on.csv - Capture when turning the heat pump on (some new id's recorded)