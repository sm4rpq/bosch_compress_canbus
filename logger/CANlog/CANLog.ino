
#include <FlexCAN.h>

static uint8_t hex[17] = "0123456789abcdef";

// -------------------------------------------------------------
static void hexDump(uint8_t dumpLen, uint8_t *bytePtr)
{
  uint8_t working;
  while( dumpLen-- ) {
    working = *bytePtr++;
    Serial.write( hex[ working>>4 ] );
    Serial.write( hex[ working&15 ] );
    Serial.write(",");
  }
}


// -------------------------------------------------------------
void setup(void)
{
  while (!Serial); //Wait for connect from serial monitor or terminal
  
  Serial.println(F("Hello Teensy CAN Test."));
  delay(2000);
  Can0.begin(125000);

  CAN_filter_t allPassFilter;

  allPassFilter.id=0;
  allPassFilter.ext=1;
  allPassFilter.rtr=0;

  for (uint8_t i=4; i<16; i++)
  {
    Can0.setFilter(allPassFilter,i);
  }

  Serial.println("Time Stamp,ID,Extended,Bus,LEN,D1,D2,D3,D4,D5,D6,D7,D8");
  /*
  CAN_message_t outMsg;
  outMsg.id=0x7df;
  outMsg.flags.extended=0;
  outMsg.flags.remote=0;
  outMsg.len=8;
  outMsg.buf[0]=0x02;
  outMsg.buf[1]=0x01;
  outMsg.buf[2]=0x00; //PID Supported - (No response)
  outMsg.buf[2]=0x0c; //Engine RPM
  outMsg.buf[3]=0xcc;
  outMsg.buf[4]=0xcc;
  outMsg.buf[5]=0xcc;
  outMsg.buf[6]=0xcc;
  outMsg.buf[7]=0xcc;
  Can0.write(outMsg);
  Serial.println("Query sent");
  */
}

// -------------------------------------------------------------
void loop(void)
{
  CAN_message_t inMsg;
  while (Can0.available()) 
  {
    Can0.read(inMsg);
    //if (inMsg.id!=0x020 && inMsg.id!=0x1c3 && inMsg.id!=0x1c8 && inMsg.id!=0x228)
    {
    Serial.print(millis());
    Serial.print(",");
    Serial.print(inMsg.id,HEX);
    if (inMsg.ext)
      Serial.print(",true,0,");
    else
      Serial.print(",false,0,");
    Serial.print(inMsg.len);
    Serial.print(",");
    hexDump(inMsg.len, inMsg.buf);
    Serial.println();
    }
  }
}
