#include <FlexCAN.h>
#include <map>
#include <LiquidCrystal.h>
#include <Metro.h>
#include <SD.h>
#include <TimeLib.h>
#include <ADC.h>
#include "vt_driver.h"
#include "bessel.h"

const int rs = 24, rw = 25, en = 26, d4 = 27, d5 = 28, d6 = 31, d7 = 32;
LiquidCrystal lcd(rs, rw, en, d4, d5, d6, d7);
Metro timer = Metro(200);
Metro sampler = Metro(120000/50);
vtDriver terminal(&Serial,80);

ADC adc;

std::map<int,CAN_message_t> messages;
float T1;
float T2;
float T3;
float T8; //Värmebärare ut
float T9; //Värmebärare in
float T10; //Köldbärare in;
float T11; //Köldbärare ut;
float D1_9; //Värmebärare diff
float D10_11; //Köldbärare diff

bool E1=false; //Kompressor (0x0048270)
bool E2_1=false; //Eltillskott steg 1 (0x003c270)
bool E2_2=false; //Eltillskott steg 2 (0x004c270)
int E2=0; //Eltillskott total
enum {heat,water} Q21;
bool G2=false; //Värmebärarpump (0x002C270)
bool G3=false; //Köldbärarpump (0x0054270)

int oldMinute = -1;

static uint8_t hex[17] = "0123456789abcdef";

File dataFile;

bool sdEnabled = false;
bool serEnabled = true;

int messageCount = 0;
char filename[20];

const float adc_factor = 1.195/4095*1000;  //Will give number of millivolts applied to A-D input
const float tslope = 3.300;
const float toffs = 273.15;
float ad590;
Bessel filter;

// -------------------------------------------------------------
void setup(void)
{
  //Use internal bandgap reference for ADC 0
  pinMode(A15, INPUT_DISABLE);
  adc.adc0->setReference(ADC_REFERENCE::REF_1V2);
  adc.adc0->setResolution(12);
  adc.adc0->setAveraging(16); // set number of averages
  adc.adc0->setConversionSpeed(ADC_CONVERSION_SPEED::VERY_LOW_SPEED); // change the conversion speed

  //Digital pin to terminate when serial port has failed, which it seems to do too often
  pinMode(5,INPUT_PULLUP);
  
  // set up the LCD's number of columns and rows:
  lcd.begin(20, 4);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("CAN-Logger bosch ver");
  lcd.setCursor(0,1);
  lcd.print("Start serial monitor");
  lcd.setCursor(0,2);
  lcd.print("to set clock");

  while (!Serial); //Wait for connect from serial monitor or terminal

  terminal.cls();
  Serial.print("Date&Time (yyyymmddhhmm) ?");
  String str;
  //while (str.length()!=12) //TODO: Fix date check
  {
    str = terminal.getInputLine();
  }
  Serial.println();
  int yr=str.substring(0,4).toInt();
  int mo=str.substring(4,6).toInt();
  int da=str.substring(6,8).toInt();
  int ho=str.substring(8,10).toInt();
  int mi=str.substring(10,12).toInt();
  setTime(ho,mi,0,da,mo,yr);

  Serial.print("Initializing SD card...");
  // see if the card is present and can be initialized:
  if (!SD.begin(BUILTIN_SDCARD)) {
    Serial.println("Card failed, or not present");
    Serial.println("Save of logdata disabled");
  }
  else
  {
    sdEnabled=true;
    Serial.println("card initialized.");
  }

  sprintf(filename,"%04d%02d%02d%02d%02d.csv",yr,mo,da,ho,mi);
  Serial.print("Will log to file: ");
  Serial.println(filename);
  dataFile = SD.open(filename,FILE_WRITE);
  dataFile.println("Time Stamp,ID,Extended,Bus,LEN,D1,D2,D3,D4,D5,D6,D7,D8");

  Serial.println();
  Serial.println("press [g] to start logging");
  bool go = false;
  do {
    char c=Serial.read();
    if (c=='g')
      go=true;
  } while (!go);
  Serial.println("press [q] to stop logging to serial");
  Serial.println("press [s] to start logging to serial");
//  Serial.println("press [f] to switch logfile");
  
  Can0.begin(125000);


  CAN_filter_t allPassFilter;

  allPassFilter.id=0;
  allPassFilter.ext=1;
  allPassFilter.rtr=0;

  for (uint8_t i=6; i<16; i++)
  {
    Can0.setFilter(allPassFilter,i);
  }

  Serial.println("Time Stamp,ID,Extended,Bus,LEN,D1,D2,D3,D4,D5,D6,D7,D8");
  lcd.clear();
  lcd.setCursor(0,0); lcd.print("Ute=     Vv =");
  lcd.setCursor(0,1); lcd.print("Frm=     Bin=");
  lcd.setCursor(0,2); lcd.print("Ret=     But=");
  lcd.setCursor(0,3); lcd.print("Dif=     Dif=");
}

// -------------------------------------------------------------

static void hexDump(uint8_t dumpLen, uint8_t *bytePtr)
{
  uint8_t working;
  while( dumpLen-- ) {
    working = *bytePtr++;
    Serial.write( hex[ working>>4 ] );
    Serial.write( hex[ working&15 ] );
    Serial.write(",");
  }
}

// -------------------------------------------------------------
void printMsg(CAN_message_t& inMsg)
{
    char prefix[40];
    if (inMsg.ext)
      sprintf(prefix,"%d,%x,true,0,%d,",millis(),inMsg.id,inMsg.len);
    else
      sprintf(prefix,"%d,%x,false,0,%d,",millis(),inMsg.id,inMsg.len);

    uint8_t len = strlen(prefix);
    for (int i=0;i<inMsg.len;i++)
    {
        uint8_t b = inMsg.buf[i];
        prefix[len+i*3]=( hex[ b>>4 ] );
        prefix[len+i*3+1]=( hex[ b&15 ] );
        prefix[len+i*3+2]=',';
    }
    prefix[len+inMsg.len*3]=0;

    if (serEnabled && Serial)
      Serial.println(prefix);
    if (sdEnabled)
    {
      dataFile.println(prefix);
      messageCount++;
      if (messageCount==20)
      {
        dataFile.close();
        dataFile = SD.open(filename,FILE_WRITE);
        messageCount=0;
      }
    }
}

// -------------------------------------------------------------

float DAToTemp(int DAValue)
{
  double RNorm = -DAValue/(DAValue-1023.0);
  double logRNorm = log(RNorm);
  return 1.0/(0.00335386157074791+0.000257897231045099*logRNorm+2.97060483563822E-6*logRNorm*logRNorm)-273.15; 
}

// -------------------------------------------------------------

void setClear(int c, int r)
{
  lcd.setCursor(c,r);
  lcd.print("     ");
  lcd.setCursor(c,r);
}

// -------------------------------------------------------------

void parse(CAN_message_t& inMsg)
{
      switch(inMsg.id)
      {
        case 0x002C270 : //En bit = Heat fluid pump = Värmebärarpump?
          if (inMsg.buf[0])
            G2=true;
          else
            G2=false;
          break;
        case 0x0038270 : //En bit? = Three-way valve = växelventil
          if (inMsg.buf[0])
            Q21=water;
          else
            Q21=heat;
          break;
        case 0x003c270 : //En bit? = Additional heat 1 = Tillstkottsvärme steg 1
          if (inMsg.buf[0])
            E2_1=true;
          else
            E2_1=false;
          E2=0;
          if (E2_1) E2+=1;
          if (E2_2) E2+=2;
          break;
        case 0x0048270 : //En bit = Kompressor
          if (inMsg.buf[0])
            E1=true;
          else
            E1=false;
          break;
        case 0x004c270 : //En bit? = Additional heat 2 = Tillskottsvärme steg 2
          if (inMsg.buf[0])
            E2_2=true;
          else
            E2_2=false;
          E2=0;
          if (E2_1) E2+=1;
          if (E2_2) E2+=2;
          break;
        case 0x0054270 : //En bit = Cold fluid pump = Cirkulationspump borrhål
          if (inMsg.buf[0])
            G3=true;
          else
            G3=false;
          break;
        case 0x8000270 :
          T1=DAToTemp(inMsg.buf[0]*256+inMsg.buf[1]);
          if (G2 && (Q21==heat))
            D1_9=T1-T9;
          break;
        case 0x8004270 :
          T2=DAToTemp(inMsg.buf[0]*256+inMsg.buf[1]);
          break;
        case 0x8008270 : 
          T3=DAToTemp(inMsg.buf[0]*256+inMsg.buf[1]);
          break;
        case 0x8014270 :
          T8=DAToTemp(inMsg.buf[0]*256+inMsg.buf[1]);
          break;
        case 0x8018270 :
          T9=DAToTemp(inMsg.buf[0]*256+inMsg.buf[1]);
          if (G2 && (Q21==heat))
            D1_9=T1-T9;
          break;
        case 0x801C270 :
          T10=DAToTemp(inMsg.buf[0]*256+inMsg.buf[1]);
          if (G3)
            D10_11=T10-T11;
          break;
        case 0x8020270 :
          T11=DAToTemp(inMsg.buf[0]*256+inMsg.buf[1]);
          if (G3)
            D10_11=T10-T11;
          break;
        case 0x0090270 : ; //8-bits = Heat fluid pump control = Varvtalsstyrning cirulationspump
          break;
      }
}

// -------------------------------------------------------------

void updateCurrent(int message)
{
      switch(message)
      {
        case 0x002C270 : //En bit = Heat fluid pump = Värmebärarpump?
          lcd.setCursor(19,0);
          if (G2)
            lcd.print("V");
          else
            lcd.print(" ");
          break;
        case 0x0038270 : //En bit? = Three-way valve = växelventil
          lcd.setCursor(19,3);
          if (Q21==water)
            lcd.print("v");
          else
            lcd.print("e");
          break;
        case 0x003c270 : //En bit? = Additional heat 1 = Tillstkottsvärme steg 1
        case 0x004c270 : //En bit? = Additional heat 2 = Tillskottsvärme steg 2
          //E2 visar effektcombo
          break;
        case 0x0048270 : //En bit = Kompressor
          lcd.setCursor(19,1);
          if (E1)
            lcd.print("C");
          else
            lcd.print(" ");
          break;
        case 0x0054270 : //En bit = Cold fluid pump = Cirkulationspump borrhål
          lcd.setCursor(19,2);
          if (G3)
            lcd.print("K");
          else
            lcd.print(" ");
          break;
        case 0x8000270 :
          setClear(4, 1);
          lcd.print(T1,1);
          if (G2 && (Q21==heat))
          {
            setClear(4, 3);
            lcd.print(D1_9,1);
          }
          break;
        case 0x8004270 :
          setClear(4, 0);
          if (T2>-10.0)
            lcd.print(T2,1);
          else
            lcd.print(T2,0);
          break;
        case 0x8008270 : 
          setClear(13, 0);
          lcd.print(T3,1);
          break;
        case 0x8014270 : //T8
          break;
        case 0x8018270 :
          setClear(4, 2);
          lcd.print(T9,1);
          if (G2 && (Q21==heat))
          {
            setClear(4, 3);
            lcd.print(D1_9,1);
          }
          break;
        case 0x801C270 :
          setClear(13, 1);
          lcd.print(T10,1);
          break;
        case 0x8020270 :
          setClear(13, 2);
          lcd.print(T11,1);
          if (G3)
          {
            setClear(13, 3);
            lcd.print(D10_11,1);
          }
          break;
        case 0x0090270 : ; //8-bits = Heat fluid pump control = Varvtalsstyrning cirulationspump
          break;
      }
}

// -------------------------------------------------------------

void updateStat(int message)
{
  
}

// -------------------------------------------------------------

void updateDisplay(int message)
{
  updateCurrent(message);  
}

// -------------------------------------------------------------

void terminate()
{
  for(auto row : messages)
  {
    printMsg(row.second);
  }
  dataFile.close();
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Logging stopped");
  lcd.setCursor(0,1);
  lcd.print("Press b to reboot");
  Serial.println("Logging stopped");
  Serial.println("Do an external reboot!");
  while(true);
}

// -------------------------------------------------------------

void loop(void)
{
  CAN_message_t inMsg;
  if (Can0.available()) 
  {
    Can0.read(inMsg);
    bool mustPrintNew = false;
    bool mustPrintOld = false;
    CAN_message_t old;
    if (messages.count(inMsg.id)==0)
    {
      //When this id is not already known it should always be printed
      mustPrintNew=true;
    }
    else
    {
      CAN_message_t old = messages[inMsg.id];
      for (int i=0; i<old.len; i++)
      {
        if (old.buf[i]!=inMsg.buf[i])
        {
          //This message is different than the previous with the same id, it must be printed
          mustPrintNew=true;
          mustPrintOld=true;
        }
      }
    }

    //Special case, two byte messages are continous information and the old should not be printed again.
    if (inMsg.len==2)
      mustPrintOld=false;

    //Remember for "future" duplicate removal
    messages[inMsg.id]=inMsg;
    
    if (mustPrintOld)
      printMsg(old);

    if (mustPrintNew)
    {
      printMsg(inMsg);
      parse(inMsg);
      updateDisplay(inMsg.id);
    }
  }
  if (sampler.check() == 1)
  {
    ad590 = filter.fir(adc.adc0->analogRead(A15)*adc_factor/tslope-toffs);
  }
  if (timer.check() == 1)
  {
    if (oldMinute!=minute())
    {
      CAN_message_t fake;
      
      //Log temperature
      fake.id=1;
      fake.len=1;
      fake.buf[0]=(uint8_t)(ad590*10);
      printMsg(fake);

      //Log time
      fake.id=0;
      fake.len=5;
      fake.buf[0]=month();
      fake.buf[1]=day();
      fake.buf[2]=hour();
      fake.buf[3]=minute();
      fake.buf[4]=second();
      printMsg(fake);
      oldMinute=minute();

    }
  }
  if (Serial.available())
  {
    char c=Serial.read();
    switch(c)
    {
      case 'x' : terminate();
                 break;
      case 'q' : serEnabled=false;
                 break;
      case 's' : serEnabled=true;
                 break;
      default:   break;
    }
  }
  if (digitalRead(5)==0)
  {
    terminate();
  }
}
