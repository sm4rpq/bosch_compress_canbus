# bosch_compress_canbus

## Description
In my house there is a Bosch Compress 5000 geothermal heat pump. Inside this there is a CAN bus between the different pcb's. I want to
know what is on this bus so that I can log what is going on. The logging is actually a long time goal and this project is just here
to find out what messages are passed and to understand what data they contain.

## Tools
The following tools were used.
* Teensy 3.5 with teensyduino
* Borland (yes) Delphi 6
* Savvycan
* gnuplot
* OpenOffice

## Roadmap
Unfortunately I always have too many ideas and projects going on so there is no actual roadmap for what should be added and when. If you
have your own ideas, please feel free to copy things freely from this project and use for whatever you like.

## Contributing
If you want to contribute information you are mostly welcome. Please send a message.

## Authors and acknowledgment
When doing some of the message identifications information from two threads on the swedish 'varmepumpsforum' have been very helpful.
So thanks to all those who have contribute to those threads.
https://www.varmepumpsforum.com/vpforum/index.php?topic=30657.0
https://www.varmepumpsforum.com/vpforum/index.php?topic=70203.0

## License
All material in this project is released under MIT license. That is, use for whatever you want however you want.

## Project status
This project will be updated as new information is derived or when my software packages are updated. No time schedule exists.
